<%--
  Created by IntelliJ IDEA.
  User: shubq
  Date: 24/03/2023
  Time: 22:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <style>.error { color: red; } .success { color: green; }</style>
</head>
<body>
<h1><%= "Login" %>
</h1>
<br/>
<form action="Connexion" method="post">
    <label> Email </label> <input type="email" id="email" name="email" required /> <br>
    <label> Password </label> <input type="password" id="psw" name="psw" required /><br/>
    <input type="submit" value="Submit"> </form>
    <span class="error">${message}</span>
<text>No account yet ?</text><a href="./index.jsp">Register</a>
</body>
</html>
