<%@ page import="fr.utc.sr03.td2_3.User" %>
<%@ page import="fr.utc.sr03.td2_3.UserManager" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Register" %>
</h1>
<br/>
<a href="HelloServlet">Hello Servlet</a>
<a href="./login.jsp">Login Page</a>
<%
    request.setAttribute("showGetUserListButton", "hidden");
    String login = null;
    session = request.getSession();
    if(session != null){
        if(session.getAttribute("login") != null) login = (String)session.getAttribute("login");
    }
    if(login!=null && UserManager.getPermissionLevel(login)){
        request.setAttribute("showGetUserListButton", "submit");
    }
%>
<form action="UserManager" method="post">
    <label> First name </label> <input type="text" id="frname" name="frname" required /><br/>
    <label> Family name </label> <input type="text" id="faname" name="faname" required /><br/>
    <label> Email </label> <input type="email" id="email" name="email" required /> <br>
    <label> Password </label> <input type="password" id="psw" name="psw" required /><br/>
    <label> male </label> <input type="radio" id="male" name="gender" value="male" checked/><br/>
    <label> female </label> <input type="radio" id="female" name="gender" value="female"/> <br/>
    <label> admin </label> <input type="radio" id="admin" name="isadmin" value="admin"/><br/>
    <label> user </label> <input type="radio" id="user" name="isadmin" value="user" checked/> <br/>
    <input type="submit" value="Submit"> </form>

<form action="UserManager" method="get">
    <input type=${showGetUserListButton} name="infoget" value="Show User List" />
</form>
<input type="hidden" >
</body>
</html>