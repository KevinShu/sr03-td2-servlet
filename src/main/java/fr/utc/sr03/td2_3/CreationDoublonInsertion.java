package fr.utc.sr03.td2_3;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CreationDoublonInsertion", value = "/CreationDoublonInsertion")
public class CreationDoublonInsertion extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Doublon!.</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Insertion(add) complete.</h1>");
            out.println("<a href=\"index.jsp\">Page Main</a>");
            out.println("</body>");
            out.println("</html>");
        }
        User temp = new User();
        temp.setFirstName(request.getParameter("frname"));
        temp.setFamilyName(request.getParameter("faname"));
        temp.setPassword(request.getParameter("psw"));
        temp.setEmail(request.getParameter("email"));
        temp.setGender(request.getParameter("gender"));
        System.out.println("Création de nouvelle utilisateur : " + temp.toString());
        UserManager.usersTable.put(UserManager.usersTable.size()+1, temp);
    }
}
