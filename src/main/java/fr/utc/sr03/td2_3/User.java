package fr.utc.sr03.td2_3;

public class User {
    private String first_name;
    private String family_name;
    private String email;
    private String password;
    private String gender;

    private boolean is_admin;

    public User(){
        this.email = "";
        this.family_name = "";
        this.first_name = "";
        this.gender = "";
        this.password = "";
        this.is_admin = false;
    }

    public String toString() {
        return "Name: " + this.first_name + " " + this.family_name + "\n" +
                "Email: " + this.email + "\n" +
                "Gender: " + this.gender + "\n" +
                "Role: " + (this.is_admin?"admin":"user") + "\n";
    }

    public void setEmail(String email){
        this.email = email;
    }

    public void setFamilyName(String family_name){
        this.family_name = family_name;
    }

    public void setFirstName(String first_name){
        this.first_name = first_name;
    }

    public void setGender(String gender){
        this.gender = gender;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public void setIs_admin(boolean value){
        this.is_admin = value;
    }

    public String getEmail(){
        return this.email;
    }

    public String getFamilyName(){
        return this.family_name;
    }

    public String getFirstName(){
        return this.first_name;
    }

    public String getGender(){
        return this.gender;
    }

    public String getPassword(){
        return this.password;
    }

    public boolean getIs_admin(){
        return is_admin;
    }

}
