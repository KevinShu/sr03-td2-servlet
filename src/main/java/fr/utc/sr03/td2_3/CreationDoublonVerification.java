package fr.utc.sr03.td2_3;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CreationDoublonVerification", value = "/CreationDoublonVerification")
public class CreationDoublonVerification extends HttpServlet {
//    HttpServletRequest originRequest;
//    HttpServletResponse originResponse;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
//        originRequest = request;
//        originResponse = response;
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Doublon!.</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>L'utilisateur qui porte le même nom existe déja.</h1>");
            out.println("<form action=\"CreationDoublonInsertion\" method=\"get\">");
            out.println("<input type=\"hidden\" name=\"frname\" value=\" " + request.getParameter("frname") + "\" />");
            out.println("<input type=\"hidden\" name=\"faname\" value=\" " + request.getParameter("faname") + "\" />");
            out.println("<input type=\"hidden\" name=\"psw\" value=\" " + request.getParameter("psw") + "\" />");
            out.println("<input type=\"hidden\" name=\"email\" value=\" " + request.getParameter("email") + "\" />");
            out.println("<input type=\"hidden\" name=\"gender\" value=\" " + request.getParameter("gender") + "\" />");
            out.println("<input type=\"submit\" name=\"add\" value=\"Add\" />\n");
            out.println("<input type=\"submit\" name=\"replace\" value=\"Replace\" />\n");
            out.println("<input type=\"submit\" name=\"cancel\" value=\"Cancel\" />\n");
            out.println("</body>");
            out.println("</html>");
        }
}
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rdInsertion = request.getRequestDispatcher("/CreationDoublonInsertion");
        RequestDispatcher rdReplace = request.getRequestDispatcher("/CreationDoublonInsertion");
        if (request.getParameter("add") != null){
            rdInsertion.forward(request, response);
        } else if (request.getParameter("replace") != null){
            rdReplace.forward(request, response);
        }
    }
}
