package fr.utc.sr03.td2_3;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicBoolean;

@WebServlet("/UserManager")
public class UserManager extends HttpServlet {
    public static final Hashtable<Integer, User> usersTable = new Hashtable<Integer, User>();

    protected static boolean checkUserNameExist(String firstName, String familyName){
        AtomicBoolean exist = new AtomicBoolean(false);
        usersTable.forEach((k, v)->{
            if (v.getFamilyName().equals(familyName)) {
                if (v.getFirstName().equals(firstName)) {
                    exist.set(true);
                }
            }
        });
        return exist.get();
    }
    protected static boolean checkLoginValid(String email, String psw){
        AtomicBoolean valid = new AtomicBoolean(false);
        usersTable.forEach((k, v)->{
            if (v.getEmail().equals(email)) {
                if (v.getPassword().equals(psw)) {
                    valid.set(true);
                }
            }
        });
        return valid.get();
    }

    public static boolean getPermissionLevel(String email){
        AtomicBoolean is_admin = new AtomicBoolean(false);
        usersTable.forEach((k, v)->{
            if (v.getEmail().equals(email)) {
                if (v.getIs_admin()) {
                    is_admin.set(true);
                }
            }
        });
        return is_admin.get();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        // Hello
        PrintWriter out = response.getWriter();
        out.println("<head><link rel=\"stylesheet\" type=\"text/css\" href=\"./css/style.css\"></head>");
        out.println("<html><body>");
        out.println("<h1>" + "Get info success !" + "</h1>");
        out.println("<table>" +
                    "<thead>" + "<tr>" + "<th colspan=\"7\">Liste d'utilisateur</th>" + "</tr>" + "</thead>" +
                    "<tbody>" +
                    "<tr>" +
                    "<td>Identifiant</td>" +
                    "<td>First Name</td>" +
                    "<td>Family Name</td>" +
                    "<td>Email</td>" +
                    "<td>Gender</td>" +
                    "<td>Password</td>" +
                    "<td>Role</td>" +
                    "</tr>");
        usersTable.forEach((k, v)->{
            out.println("<tr>" +
                    "<td>" + k + "</td>" +
                    "<td>" + v.getFirstName() + "</td>" +
                    "<td>" + v.getFamilyName() + "</td>" +
                    "<td>" + v.getEmail() + "</td>" +
                    "<td>" + v.getGender() + "</td>" +
                    "<td>" + v.getPassword() + "</td>" +
                    "<td>" + (v.getIs_admin()?"admin":"user") + "</td>" +
                    "</tr>");
        });
        out.println("</tbody>" + "</table>");
        out.println("<br/><button type=\"button\" value=\"Back\" onclick=\"history.go(-1)\" id=\"back_btn\">Back</button>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("frname").trim().equals("") || request.getParameter("faname").trim().equals("") || request.getParameter("psw").trim().equals("") || request.getParameter("email").trim().equals("") || request.getParameter("gender").trim().equals("")){
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();//get the stream to write the data
            pw.println("<html><body>");
            pw.println("<h1>" + "Failure! Check your info. " + "</h1>");
            pw.println("<br/><button type=\"button\" value=\"Back\" onclick=\"history.go(-1)\" id=\"back_btn\">Back</button>");
            pw.println("</body></html>");
            pw.close();
        }else if (checkUserNameExist(request.getParameter("frname"), request.getParameter("faname"))){

            // Ex 1,2
//            response.setContentType("text/html");
//            PrintWriter pw = response.getWriter();//get the stream to write the data
//            pw.println("<html><body>");
//            pw.println("<h1>" + "Failure! User Name already exist. " + "</h1>");
//            pw.println("<br/><button type=\"button\" value=\"Back\" onclick=\"history.go(-1)\" id=\"back_btn\">Back</button>");
//            pw.println("</body></html>");
//            pw.close();

            // Ex 3
            RequestDispatcher rd = request.getRequestDispatcher("./CreationDoublonVerification");
            rd.forward(request, response);

        }else {
            User temp = new User();
            temp.setFirstName(request.getParameter("frname"));
            temp.setFamilyName(request.getParameter("faname"));
            temp.setPassword(request.getParameter("psw"));
            temp.setEmail(request.getParameter("email"));
            temp.setGender(request.getParameter("gender"));
            temp.setIs_admin(request.getParameter("isadmin").equals("admin"));
            System.out.println("Création de nouvelle utilisateur : " + temp.toString());
            usersTable.put(usersTable.size()+1, temp);
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();//get the stream to write the data
            pw.println("<html><body>");
            pw.println("<h1>" + "Success ! User Info: " + "</h1>");
            pw.println(temp.toString());
            pw.println("<br/><button type=\"button\" value=\"Back\" onclick=\"history.go(-1)\" id=\"back_btn\">Back</button>");
            pw.println("</body></html>");
            pw.close();
        }
    }
}
