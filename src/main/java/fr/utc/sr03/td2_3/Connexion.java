package fr.utc.sr03.td2_3;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

import static java.lang.Thread.sleep;

@WebServlet(name = "Connexion", value = "/Connexion")
public class Connexion extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("psw").trim().equals("") || request.getParameter("email").trim().equals("")){
            response.setContentType("text/html");
            PrintWriter pw = response.getWriter();//get the stream to write the data
            pw.println("<html><body>");
            pw.println("<h1>" + "Failure! Check your login ou password. " + "</h1>");
            pw.println("<br/><button type=\"button\" value=\"Back\" onclick=\"history.go(-1)\" id=\"back_btn\">Back</button>");
            pw.println("</body></html>");
            pw.close();
        }else if (UserManager.checkLoginValid(request.getParameter("email"), request.getParameter("psw"))){
            HttpSession session = request.getSession(false);  // returns null if no session or session is invalid
            if(session != null) {
                // you have old session
                session.invalidate();  // invalidate session - this will remove any old attrs hold in the session
            }
            // create new session
            session = request.getSession(); // creates new empty session
            if (UserManager.getPermissionLevel(request.getParameter("email"))){
                session.setAttribute("login", request.getParameter("email"));
                session.setAttribute("is_admin", true);
                // response.sendRedirect("./index.jsp");
                PrintWriter pw = response.getWriter();//get the stream to write the data
                pw.println("<!DOCTYPE html>");
                pw.println("<html><head><title>Navigation</title></head>");
                pw.println("<body>");
                pw.println("<h1>Hello, administrator " + session.getAttribute("login") + "</h1>");
                pw.println("<nav> <ul>");
                pw.println(" <li>Connected</li>");
                pw.println("<li><a href=\"./index.jsp\">Management utilisateurs</a></li>");
                pw.println("<li><a href=\"./home.jsp\">Home page</a></li>");
                pw.println("</ul>");
                pw.println("</nav>");
                pw.println("</body>");
                pw.println("</html>");
            } else {
                session.setAttribute("login", request.getParameter("email"));
                session.setAttribute("is_admin", false);
                // response.sendRedirect("./index.jsp");
                PrintWriter pw = response.getWriter();//get the stream to write the data
                pw.println("<html><body>");
                pw.println("<h1>" + "Welcome back, " + session.getAttribute("login") + " !" + "</h1>");
                pw.println("<h3>" + "Login successful." + "</h3>");
                pw.println("<text> If not redirected automatically, </text>" + "<a href=\"./home.jsp\">click here.</a>");
                pw.println("</body></html>");
                pw.close();
            }
        }else{
            request.setAttribute("message", "Identifiant ou mot de passe invalide !"); // Will be available as ${message}
            request.getRequestDispatcher("login.jsp").forward(request,response);
        }
    }
}
