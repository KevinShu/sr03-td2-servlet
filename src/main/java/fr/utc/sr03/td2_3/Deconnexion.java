package fr.utc.sr03.td2_3;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Deconnexion", value = "/Deconnexion")
public class Deconnexion extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            HttpSession session = request.getSession();
            session.invalidate();
            response.sendRedirect("./login.jsp");
            PrintWriter pw = response.getWriter();//get the stream to write the data
            pw.println("<html><body>");
            pw.println("<h1>" + "Goodbye !" + "</h1>");
            pw.println("<h3>" + "Logout successful." + "</h3>");
            pw.println("</body></html>");
            pw.close();
    }
}
